package pralka;

public class Whirpool extends Pralka {

	final protected int maxCountProgram = 25;
	
	public Whirpool(int currentProgram, double temp, int pralkaSpeed) {
		super(currentProgram, temp, pralkaSpeed);
		this.producent = Producent.WHIRPOOL;
	}
	
	@Override
	public void setCurrentProgram(int currentProgram) {
		if (currentProgram < 1 || currentProgram > maxCountProgram) {
			System.out.println("Wash Program out of range, it has be 1-" + maxCountProgram);
			this.currentProgram = this.minCountProgram;
		} else {
			this.currentProgram = currentProgram;
		}
	}
}
