package pralka;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {

		List<Pralka> washMachineList = new ArrayList<>();
		
		Beko beko = new Beko(23, 1, 950);
		Amica amica = new Amica(23, 24.74, 1);
		Whirpool whirpool = new Whirpool(26, 1, 1);
		
		try {
			beko.tempDown();
		} catch (TempDownExeption e) {
			e.printStackTrace();
		}
		System.out.println();
		washMachineList.add(beko);
		washMachineList.add(amica);
		washMachineList.add(whirpool);
		writeList(washMachineList);
		
		System.out.println(beko.showStatus());
		System.out.println(amica.showStatus());
		System.out.println(whirpool.showStatus());
		
		beko.pralkaSpeedUp();
		
		beko.previusProgram();

	
		System.out.println(beko.showStatus());
		beko.previusProgram();
		beko.tempUp();
		beko.setTemp(91);
		beko.setTemp(-1);
		System.out.println(beko.showStatus());

	}
	
	public static void writeList(List<Pralka> washMachineList) {
		System.out.println("------------------->NOT SORTED");
		washMachineList.stream().forEach(obj -> System.out.println(obj.showStatus()));
		System.out.println("------------------->SORTED");
		washMachineList.stream()
			.sorted((a ,b) -> a.getProducent().name().compareTo(b.getProducent().name()))
			.collect(Collectors.toList())
			.forEach(obj -> System.out.println(obj.showStatus()));
			System.out.println("------------------->END");
	}

}
