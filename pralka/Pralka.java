package pralka;

public abstract class Pralka {

	protected int currentProgram;
	final protected int maxCountProgram = 20;
	final protected int minCountProgram = 1;
	protected double temp;
	final protected double maxTemp = 90;
	final protected double minTemp = 1;
	protected double changeTemp = 0.5;
	protected int pralkaSpeed;
	final protected int minPralkaSpeed = 100;
	final protected int maxPralkaSpeed = 1000;
	final protected int skokSpeed = 100;
	protected Producent producent;

	public Pralka(int currentProgram, double temp, int pralkaSpeed) {
		setCurrentProgram(currentProgram);
		setTemp(temp);
		setPralkaSpeed(pralkaSpeed);
	}

	public Pralka() {
	}

	public void previusProgram() {
		if (currentProgram > minCountProgram && currentProgram <= maxCountProgram) {
			currentProgram--;
		} else {
			currentProgram = maxCountProgram;
		}
	}

	public void nextProgram() {
		if (currentProgram >= minCountProgram && currentProgram < maxCountProgram) {
			currentProgram++;
		} else {
			currentProgram = minCountProgram;
		}
	}

	public void setCurrentProgram(int currentProgram) {
		if (currentProgram < minCountProgram || currentProgram > maxCountProgram) {
			System.out.println("Wash Program out of range, it has be 1-" + maxCountProgram);
			this.currentProgram = minCountProgram;
		} else {
			this.currentProgram = currentProgram;
		}
	}

	public int getCurrentProgram() {
		return currentProgram;
	}

	public void setPralkaSpeed(int pralkaSpeed) {
		if (pralkaSpeed >= minPralkaSpeed && pralkaSpeed <= maxPralkaSpeed) {
			this.pralkaSpeed = (int) Math.round((double) pralkaSpeed / 100) * 100;
		} else {
			System.out.println("Wash machine Speed is out of range");
			this.pralkaSpeed = minPralkaSpeed;
		}
	}

	public int getPralkaSpeed() {
		return pralkaSpeed;
	}

	public void pralkaSpeedUp() {
		if (pralkaSpeed == maxPralkaSpeed) {
			pralkaSpeed = minPralkaSpeed;
		} else if (pralkaSpeed >= minPralkaSpeed && pralkaSpeed <= (maxPralkaSpeed - skokSpeed)) {
			pralkaSpeed += skokSpeed;
		}
	}

	public void pralkaSpeedDown() {
		if (pralkaSpeed == minPralkaSpeed) {
			pralkaSpeed = maxPralkaSpeed;
		} else if (pralkaSpeed >= minPralkaSpeed && pralkaSpeed <= maxPralkaSpeed) {
			pralkaSpeed -= skokSpeed;
		}
	}

	public void setTemp(double temp) {
		if (temp > maxTemp) {
			System.out.println("Can't set temperature up then 90�C, temp is:" + maxTemp);
			this.temp =maxTemp;
		} else if (temp < minTemp) {
			System.out.println("Can't set negativ temperature, temp is: " + minTemp);
			this.temp = minTemp;
		} else {
			this.temp = Math.round(temp * 2) / 2.0;
		}
	}

	public double getTemp() {
		return temp;
	}

	public void tempDown() throws TempDownExeption{
		if (temp <= minTemp) {
			throw new TempDownExeption();
		} else {
			temp -= changeTemp;
			System.out.println("Current temperature is changed to: " + temp + (char) 176 + "C");
		}
	}

	public void tempUp() {
		if (temp >= maxTemp) {
			System.out.println("Can't set temperature up then 90�C. Temperature is not changed");
		} else {
			temp += changeTemp;
			System.out.println("Current temperature is changed to: " + temp + (char) 176 + "C");
		}
	}

	public int getMaxCountProgram() {
		return maxCountProgram;
	}

//	public void setMaxCountProgram(int maxCountProgram) {
//		if (maxCountProgram < 1 && this.maxCountProgram > maxCountProgram) {
//			System.out.println("Can't set Program out of 1-" + maxCountProgram);
//		} else {
//			this.maxCountProgram = maxCountProgram;
//		}
//
//	}

	public String showStatus() {
		return "Producent: " + producent + ", Current program: " + currentProgram + ", Current temperature: " + temp
				+ ", Current wash machine speed: " + pralkaSpeed;
	}

	public Producent getProducent() {
		return producent;
	}

	public void setProducent(Producent producent) {
		this.producent = producent;
	}

}
