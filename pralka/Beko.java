package pralka;

public class Beko extends Pralka {
	
	
	
	public Beko(int currentProgram, double temp, int pralkaSpeed) {
		super(currentProgram, temp, pralkaSpeed);
		this.producent = Producent.BEKO;
		this.changeTemp = 1;
	}
	@Override
	public void setTemp(double temp) {
		if (temp > maxTemp) {
			System.out.println("Can't set temperature up then 90�C, temp is: " + maxTemp + (char) 176 + "C");
			this.temp = maxTemp;
		} else if (temp < minTemp) {
			System.out.println("Can't set negativ temperature, temp is: " + minTemp + (char) 176 + "C");
			this.temp = minTemp;
		} else {
			this.temp = Math.round(temp);
		}
	}

}
